package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Polinom;

public class Controller{

	public static void placeComponents(JPanel panel) {
		
		panel.setLayout(null);
		
		JLabel l1 = new JLabel("Polinom1: ");
		JLabel l2 = new JLabel("Polinom2: ");
		JLabel l3 = new JLabel("Rezultat:");
		final JLabel lrez = new JLabel();
		
		final JTextField t1 = new JTextField(30);
		final JTextField t2 = new JTextField(30);
	//	JTextField t3 = new JTextField(30);
		
		JButton suma=new JButton("SUMA");
		JButton scadere=new JButton("SCADERE");
		JButton inmultire=new JButton("INMULTIRE");
		JButton impartire=new JButton("IMPARTIRE");
		JButton derivare=new JButton("DERIVARE");
		JButton integrare=new JButton("INTEGRARE");
		JButton clear=new JButton("CLEAR"); 
		
		l1.setBounds(10,10,150,50);
		l2.setBounds(10,50,150,50);
		l3.setBounds(10,100,150,50);
		lrez.setBounds(10, 120, 700, 100);
		
		suma.setBounds(10,250,110,30);
		scadere.setBounds(120,250,110,30);
		inmultire.setBounds(10,300,110,30);
		impartire.setBounds(120,300,110,30);
		derivare.setBounds(10,350,110,30);
		integrare.setBounds(120,350,110,30);
		clear.setBounds(65, 400, 110, 30);
		t1.setBounds(80,23,170,25);
		t2.setBounds(80,60,170,25);
		
		suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom polinom1;
				Polinom polinom2;
				Polinom polinom3;
				
				String p1 = t1.getText();
				String p2 = t2.getText();
				
				polinom1 = new Polinom(p1);
				polinom2 = new Polinom(p2);
				polinom3 = new Polinom();
				
				polinom3 = polinom1.adunare(polinom2);
				
				lrez.setText(polinom3.getPolinom());	
			}
		});
		
		scadere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom polinom1;
				Polinom polinom2;
				Polinom polinom3;
				
				String p1 = t1.getText();
				String p2 = t2.getText();
				
				polinom1 = new Polinom(p1);
				polinom2 = new Polinom(p2);
				polinom3 = new Polinom();
				
				polinom3 = polinom1.scadere(polinom2);
				
				lrez.setText(polinom3.getPolinom());	
			}
		});
		
		inmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom polinom1;
				Polinom polinom2;
				Polinom polinom3;
				
				String p1 = t1.getText();
				String p2 = t2.getText();
				
				
				polinom1 = new Polinom(p1);
				polinom2 = new Polinom(p2);
				polinom3 = new Polinom();
				
				polinom3 = polinom1.inmultire(polinom2);
				
				lrez.setText(polinom3.getPolinom());	
			}
		});
		
		integrare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom polinom;
				String p1 = t1.getText();
				polinom = new Polinom(p1);
				polinom.integrare();
				lrez.setText(polinom.getPolinom());
			}
		});
		
		derivare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom polinom;
				String p1 = t1.getText();
				polinom = new Polinom(p1);
				polinom.derivare();
				lrez.setText(polinom.getPolinom());
			}
		});
		
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lrez.setText("");
				t1.setText("");
				t2.setText("");
			}
		});
		
		panel.add(l1);
		panel.add(l2);
		panel.add(l3);
		panel.add(lrez);
		panel.add(suma);
		panel.add(scadere);
		panel.add(inmultire);
		panel.add(impartire);
		panel.add(derivare);
		panel.add(integrare);
		panel.add(clear);
		panel.add(t1);
		panel.add(t2);
	}
}
