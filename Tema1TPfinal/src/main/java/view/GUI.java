package view;
import javax.swing.*;
import controller.Controller;
public class GUI extends Controller {

	public static void main(String[] args) {  
		JFrame fr=new JFrame();
		fr.setSize(800,500);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fr.setTitle(" CalculatorPolinoame :)");
		JPanel panel = new JPanel();
		fr.add(panel);
		placeComponents(panel);
		fr.setVisible(true);
	}
}

