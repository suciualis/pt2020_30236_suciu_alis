package model;

import java.util.ArrayList;

public class Polinom{
	
	String polinom;
	ArrayList<Monom> monom = new ArrayList<Monom>();
	
	public Polinom(String polinom) {
		String[] StrMon;
		this.polinom = polinom;
		String buffer = this.polinom;
		StrMon = buffer.split("(?=[+-])");
		for (String m : StrMon) {
			monom.add(new Monom(m));
		}
	}
	
	public Polinom() {
		// TODO Auto-generated constructor stub
	}
	public ArrayList<Monom> getMonoms(){
		return this.monom;
	}
	
	public String getPolinom() {
		String p = "";
		for (int i = 0;i<monom.size();i++) {
			if (monom.get(i).getCoeficient() < 0) {
				p += monom.get(i).getMonom();
			}
			else {
				p += (0 == i)? monom.get(i).getMonom():" + "+monom.get(i).getMonom();
			}
		}
		return p;
	}
	
	
	public Polinom adunare(Polinom pol2) {
		Polinom p3 = new Polinom();
		int i=0;
		int j=0;
		
		while ((i<this.monom.size())&&(j<pol2.monom.size())) {
			int p1 = this.monom.get(i).getPutere();
			double c1 = this.monom.get(i).getCoeficient();
			
			int p2 = pol2.monom.get(j).getPutere();
			double c2 = pol2.monom.get(j).getCoeficient();
			
			int put1 = pol2.getPutere(p1);
			int put2 = this.getPutere(p2);
			
			if (put1 == -1) {
				p3.monom.add(new Monom(c1,p1));
				i++;
			}
			else if (put2 == -1) {
				p3.monom.add(new Monom(c2,p2));
				j++;
			}
			else {
				int put3 = pol2.monom.get(put1).getPutere();
				double coef3 = pol2.monom.get(put1).getCoeficient();
				
				p3.monom.add(new Monom(c1+coef3,put3));
				i++;
			}
		}
		return p3;
		}
	public Polinom scadere(Polinom pol2) { 
		Polinom pol3 = new Polinom();
		for (int i=0;i<pol2.monom.size();i++) {
			pol2.monom.get(i).setCoeficient(-pol2.monom.get(i).getCoeficient());
		}
		pol3 = this.adunare(pol2);
		return pol3;
	}
		
	private int getPutere(int putere) {
		for (int i = 0;i<monom.size();i++) {
			Monom mon = monom.get(i);
			if (putere == mon.getPutere())
				return i;
		}
			return -1;
	}
	
	public Polinom inmultire(Polinom pol2) {
		Polinom pol3 = new Polinom();
		
		for (int i = 0;i<this.monom.size();i++) {
			Monom m1 = this.monom.get(i);
			for (int j=0;j<pol2.monom.size();j++) {
				Monom m2 = pol2.monom.get(j);
				Monom m3 = m1.inmultire(m2);
				pol3.monom.add(m3);
			}
		}
		int i = 0;
		while (i<pol3.monom.size()) {
			int put1 = pol3.monom.get(i).getPutere();
			int j=i+1;
			while (j<pol3.monom.size()) {
				if (put1 == pol3.monom.get(j).getPutere()) {
					double c1 = pol3.monom.get(i).getCoeficient();
					double c2 = pol3.monom.get(j).getCoeficient();
					double c3 = c1+c2;
					pol3.monom.get(i).setCoeficient(c3);
					pol3.monom.remove(j);
				}
				j++;
			}
			i++;
		}
		return pol3;
	}
	public Polinom integrare() {
		for (Monom m: monom) {
			m.integrare();
		}
		return this;
	}
	public Polinom derivare() {
		for (Monom m: monom) {
			m.derivare();
		}
		return this;
	}
}

