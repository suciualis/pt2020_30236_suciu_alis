package model;

import java.util.regex.Pattern;
import java.text.DecimalFormat;
import java.util.regex.Matcher;

public class Monom {

	private String monom;
	private int putere;
	private double coeficient;
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	public Monom(String n_monom) {
		formMon(n_monom);
		monom = n_monom; 
	}
	public Monom(double coef, int put) {
		putere = put;
		coeficient = coef;
		monom = form();
	}
	
	public String getMon() {
		return monom;
	}
	public String getMonom() {
		return (putere == 0)? Double.toString(coeficient): (df2.format(coeficient) +" x^"+putere);
	}
	public int getPutere() {
		return putere;
	}
	public double getCoeficient() {
		return coeficient;
	}
	public void setMonom(String n_monom) {
		monom = n_monom;
	}
	public void setPutere(int n_putere) {
		putere = n_putere;
	}
	public void setCoeficient(double n_coeficient) {
		coeficient = n_coeficient;
	}
	private String form() {
		String m = new String (this.coeficient+"x^"+this.putere);
		return m;
	}
	private void formMon(String monom) {
		String[] buf;
		Pattern formatPol = Pattern.compile("\\^");
		Matcher m = formatPol.matcher(monom);
		String a = new String();
		while (m.find()) {
			a = m.group(); //grupez dupa ^ 
		}
		if (a.isEmpty()) { //nu contine ^
			buf =monom.split("x");
			if (buf.length == 0) { // x
				coeficient = 1;
				putere = 1;
			}
			else {
				coeficient = (buf[0].isEmpty())? 1:Integer.parseInt(buf[0]);
				putere = (buf[0] == monom)? 0 : 1;
			}
		} else {
				buf = monom.split("\\^");
				String nrStr = new String();
				for (int i = 0; i<buf[0].length();i++) {
					char c = buf[0].charAt(i);
					if (c== 45) //este -
						nrStr = nrStr + c;
					if (c > 47 && c < 58) //este cifra
						nrStr = nrStr + c;
				}
				coeficient = (nrStr.isEmpty()? 1 : Integer.parseInt(nrStr));
				putere = Integer.parseInt(buf[1]);		
		}
	}

	public Monom inmultire(Monom mon1) {
		Monom rez;
		double c = this.coeficient*mon1.coeficient;
		int p = this.getPutere()+mon1.getPutere();
		rez = new Monom(c,p);
		return rez;
	}
	public Monom derivare() {
		if (putere!=0) {
			coeficient = coeficient *putere;
			putere = putere - 1;
		}
		else {
			coeficient=0;
			putere=0;
		}
		Monom rez = new Monom(coeficient,putere);
		return rez;
	}
	public Monom integrare() {
		putere++;
		coeficient = coeficient / putere;
		Monom rez = new Monom(coeficient,putere);
		return rez;
	}
}

